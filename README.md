# Projeto Autenticação  #

O que é o projeto Autenticação?

O projeto Autenticação foi desenvolvido com o objetivo de integrar maneiras diferentes de autenticação que possa dar aos usuários
maior segurança durante a navegação.
O projeto foi elaborado utilizado boas práticas de programação, baseado nos conceitos de 'Clean Code' e SOLID e claro, .NET.

### O que é necessário para utilizar? ###

* O projeto foi desenvolvido com a IDE Visual Studio 2017 (version 15) e .NET Framework SDK 

O ambiente para desenvolvimento escolhido, foi o Windows 10 da Microsoft, porem, utilizando .NET Core, podemos migrar a solução para o novo SDK da microsoft e 
trabalhar em qualquer ambiente(MAC, Linux ou Windows).

### Como o controle de autenticação é realizado? ###

O projeto foi desenvolvido utilizando o membership da Microsoft Asp.Net Identity, e foi utilizado para os seguintes objetivos.

* Um sistema de identidade ASP.NET
Identidade do ASP.NET pode ser usada com todas as estruturas ASP.NET, como ASP.NET MVC, Web Forms, Web Pages, Web API e SignalR.

* Facilidade de conexão de dados de perfil sobre o usuário
Você controla o esquema de informações de usuário e perfil. Por exemplo, você pode facilmente habilitar o sistema para armazenar datas de nascimento inseridas pelos usuários quando eles registrarem uma conta em seu aplicativo.

* Controle de persistência
Por padrão, o sistema ASP.NET Identity armazena todas as informações do usuário em um banco de dados. ASP.NET Identity usa Entity Framework Code First para implementar todo o mecanismo de persistência.
Uma vez que você controla o esquema do banco de dados, tarefas comuns, como alterar nomes de tabela ou alterar o tipo de dados de chaves primárias, são fáceis de fazer.

* Role provider
Existe um fornecedor de funções que permite restringir o acesso a partes do seu aplicativo por funções. Você pode facilmente criar funções como "Admin" e adicionar usuários a funções.

* Social Login Providers
Você pode facilmente adicionar logs sociais, como conta da Microsoft, Facebook, Twitter, Google e outros, ao seu aplicativo e armazenar os dados específicos do usuário em sua aplicação.


### Será que é difícil consfigurar tudo isso? ###

* Não mesmo!!
Toda configuração e estrutura do Asp.Net Identity pode ser vista <a href="https://docs.microsoft.com/en-us/aspnet/identity/overview/getting-started/introduction-to-aspnet-identity">Aqui</a> no site do próprio membership.
Agora é só baixar e usar.