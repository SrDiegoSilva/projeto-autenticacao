namespace ProjetoAutenticacaoMax.Migrations
{
    using System.Data.Entity.Migrations;

    internal sealed class Configuration : DbMigrationsConfiguration<ProjetoAutenticacaoMax.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(ProjetoAutenticacaoMax.Models.ApplicationDbContext context)
        {
        }
    }
}
