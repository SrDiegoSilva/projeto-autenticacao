﻿using System.Configuration;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Twilio;

namespace ProjetoAutenticacaoMax.Models
{
    public class SmsService : IIdentityMessageService
    {
        public Task SendAsync(IdentityMessage message)
        {
            if (ConfigurationManager.AppSettings["Internet"] == "true")
            {
                // Utilizando TWILIO como SMS Provider.
                // https://www.twilio.com/docs/quickstart/csharp/sms/sending-via-rest

                const string accountSid = "AC9be78efe7b5760efa3a5d3f685d58878";
                const string authToken = "3a450f17cd4fc2594ef3aee4a5c9db68";

                var client = new TwilioRestClient(accountSid, authToken);
                
                client.SendMessage("+55 81 4042-1162", message.Destination, message.Body);
            }

            return Task.FromResult(0);
        }
    }
}