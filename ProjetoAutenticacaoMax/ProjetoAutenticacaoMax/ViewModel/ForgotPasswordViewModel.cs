﻿using System.ComponentModel.DataAnnotations;

namespace ProjetoAutenticacaoMax.ViewModel
{
    public class ForgotPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }
}