﻿namespace ProjetoAutenticacaoMax.ViewModel
{
    public class ResendValidationEmailViewModel
    {
        public string UserId { get; set; }

        public string CallbackUrl { get; set; }
    }
}