﻿using System.ComponentModel.DataAnnotations;

namespace ProjetoAutenticacaoMax.ViewModel
{
    public class ForgotViewModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }
}