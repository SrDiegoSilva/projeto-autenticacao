﻿using System.Web.Mvc;
using ProjetoAutenticacaoMax.Filters;

namespace ProjetoAutenticacaoMax
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
            filters.Add(new LogFIlter());
        }
    }
}
