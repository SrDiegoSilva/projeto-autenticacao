﻿using System;
using System.IO;

namespace ProjetoAutenticacaoMax.Log
{
    public static class LogTextHelper
    {
        private static string _path = @"C:\Logs\log.txt";

        public static void AdicionarLog(string msg)
        {
            if (!File.Exists(_path))
            {
                File.Create(_path);
                TextWriter tw = new StreamWriter(_path);
                tw.WriteLine(DateTime.Now + " - LOG MESSAGE: " + msg);
                tw.Close();
            }
            else
            {
                TextWriter tw = File.AppendText(_path);
                tw.WriteLine(DateTime.Now + " - LOG MESSAGE: " + msg);
                tw.Close();
            }
        }

    }
}