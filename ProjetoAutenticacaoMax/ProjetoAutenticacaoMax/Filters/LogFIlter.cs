﻿using System.Web;
using System.Web.Mvc;
using ProjetoAutenticacaoMax.Log;

namespace ProjetoAutenticacaoMax.Filters
{
    public class LogFIlter : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var controller = (string)filterContext.RouteData.Values["controller"];
            var action = (string)filterContext.RouteData.Values["action"];
            var url = $"/{controller}/{action}";
            var mensagem = $"O usuário {HttpContext.Current.User.Identity.Name} acessou a página {url}";
            LogTextHelper.AdicionarLog(mensagem);
        }
    }
}